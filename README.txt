README
--------------------------------------------------------------------------
This module allows you to define a custom menu (toolbar) for managing and editing the site by the customer (editor).

INSTALLATION
--------------------------------------------------------------------------
1. Copy the client_menu folder to your modules directory
2. Go to admin > Site building > Modules, and enable this module. 

This will add :
 - A configuration page
 - 2 menus : First client menu and Second client menu
 - A toolbar for the client

-- CONTACT --

Current maintainers:
* Giovanni Baptiste (bgeto) - http://drupal.org/user/1915444